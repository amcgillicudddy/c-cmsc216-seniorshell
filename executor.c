#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <err.h>

#include "command.h"
#include "executor.h"

int aux_ex (struct tree *t , int fd_in , int fd_out);

int execute(struct tree *t) {
    aux_ex(t , 0 , 1);
    return 0;
}


/* aux func to recursively call, and keep tract of 
   file descriptors */

int aux_ex(struct tree *t , int fd_in , int fd_out){
    pid_t child , grand_child;
    int status;
    int pied_piper[2];

    /*NULL check for the tree */
    
    if(t != NULL){

	/* handles case where we need to execute command, aka no conjunction */
	
	if(t->conjunction == NONE){

	    /* handles case where we want to exit */
	    
	    if (strcmp(t->argv[0] , "exit") == 0) {
		exit(0);

		/* change directory cases */
		
	    } else if (strcmp(t->argv[0] , "cd") == 0) {

		/* case where we want to go to specified directory */
		
		if (t->argv[1] != NULL) {
		    chdir(t->argv[1]);
		} else {

		    /* home case */
		    
		    chdir(getenv("HOME"));
		}
		return 0;
	    } else {

		/*check to make sure fork does not fail */
		
		if ((child = fork()) < 0) {
		    perror("fork");
		}

		/* child code */
		
		if (child == 0){

		    /*input redirection */

		    if (t->input != NULL) {
			fd_in = open (t->input , O_RDONLY);

			/* checks to make sure file is handled correctly */
			
			if (fd_in < 0) {
			    perror("input file open error");
			}

			if (dup2(fd_in , STDIN_FILENO) < 0) {
			    perror("input dup2 error");
			}

			if (close(fd_in) < 0) {
			    perror("close input file error");
			}
			
			/*output redirection */
			
		    } else if (t->output != NULL) { 
			fd_out = open (t->output ,
				       O_RDWR | O_CREAT | O_TRUNC , 0664);

			if (fd_out < 0){
			    perror("output file open error");
			}

			if (dup2(fd_out , STDOUT_FILENO) < 0) {
			    perror("output dup2 error");
			}

			if (close(fd_out) < 0) {
			    perror("close output file error");
			}
		    }
		    
		    /* executes the command */
		    
		    execvp(t->argv[0] , t->argv);

		    /* if command fails does this */
		    
		    printf("Failed to execute %s\n" , t->argv[0]);
		    exit(1);

		    /*parent code */
		    
		} else {
		    wait(&status);
		    return status;
		}
	    }

	    /* AND conjunction */
	    
	} else if (t->conjunction == AND){

	    /* case where input and output redirection is not required */
	    
	    if (t->input == NULL && t->output == NULL) {
		child = fork();

		if (child < 0) {
		    perror("fork error");
		}

		/* child code */
		
		if (child == 0){

		    /* executes both sides of the tree */
		    
		    if (aux_ex(t->left , fd_in , fd_out) == 0) {
			if (aux_ex(t->right , fd_in , fd_out) == 0) {
			    exit(0);
			} else {
			    exit(2);
			}
		    } else {
			exit(2);
		    }

		    /* parent code */
		    
		} else {
		    wait(&status);
		    return status;
		}
	    }

	    /* input redirection */
	    
	    if(t->input != NULL) {
		child = fork();

		if (child < 0) {
		    perror("fork error - AND input");
		}

		/* child code */
		
		if (child == 0) {
		    fd_in = open(t->input , O_RDONLY);

		    if (fd_in < 0) {
			perror("input file open error in AND");
		    }

		    if (dup2(fd_in , STDIN_FILENO) < 0) {
			perror("dup2 error - input - AND");
		    }

		    if (close(fd_in) < 0) {
			perror("file close error - input");
		    }


		    /* left and right sides */
		    if(aux_ex(t->left , fd_in , fd_out) == 0) {
			if(aux_ex(t->right , fd_in , fd_out) == 0) {
			    exit(0);
			} else {
			    exit(2);
			}
		    } else {
			exit(2);
		    }

		    /* parent code */
		} else {
		    wait(&status);
		    return status;
		}
	    }


	    /* output redirection */
	    if (t->output != NULL) {
		fd_out = open(t->output , O_RDWR | O_CREAT | O_TRUNC , 0664);

		if (fd_out  < 0) {
		    perror("output file open error AND");
		}
		
		if (dup2(fd_out , STDOUT_FILENO) < 0){
		    perror("dup2 error - output - AND");
		}
		
		if (close(fd_out) < 0) {
		    perror("file close error - output");
		}
		
		child = fork();
		
		if (child < 0){
		    perror("fork - AND output");
		}

		if (child == 0) {
		    /* left and right tree */
		    if(aux_ex(t->left , fd_in , fd_out) == 0) {
			if(aux_ex(t->right , fd_in , fd_out) == 0) {
			    exit(0);
			} else {
			    exit(2);
			}
		    } else {
			exit(2);
		    }
		} else {
		    wait(&status);
		    return status;
		}
		return 0;
	    }
	    /* PIPE case */
	} else if (t->conjunction == PIPE) {
	    child = fork();

	    if (child == 0) {

		/* pipe */
		
		if (pipe (pied_piper) < 0) {
		    perror("get a plumber, and check ya pipes");
		}

		if ( (grand_child = fork()) < 0) {
		    perror("grand_child pipe error");
		}
		
		if (grand_child == 0) {

		    /* close read end of pipe */
		    
		    close(pied_piper[0]);

		    if (dup2(pied_piper[1] , STDOUT_FILENO) < 0) {
			perror("pipe error: grand_child");
		    }

		    /* go down left of tree, will be processed first */
		    
		    if (aux_ex(t->left , fd_in , fd_out) != 0) {
			exit(0);
		    } else {
			exit(2);
		    }
		} else {
		    wait (&status); /* wait for child process */

		    /* close write end of pipe not needed */
		    
		    close (pied_piper[1]);
		    
		    if ( dup2(pied_piper[0] , STDIN_FILENO) < 0) {
			perror("pipe error: child (parent of grand_child");
		    }

		    /* go down right of tree, will be processed second */
		    
		    if (aux_ex(t->right , fd_in , fd_out) != 0) {
			exit(0);
		    } else {
			exit(2);
		    }
		}
		
		/* waits for children to finish */
		
	    } else {
		int orig_stat;
		wait(&orig_stat);
		return orig_stat;
	    }
	}
    }
    return 0;
}




